# MQTT Bridge

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](LICENSE)

A MQTT bridge for forward

## Features

- [x] Forward MQTT payload
- [x] MQTT v3.1.1

## Config File

`config.json`
```json
{
  "client_id": "",
  "subscription": {
    "url": "",
    "username": "",
    "password": "",
    "cert_path": "",
    "keep_alive": 5,
    "auto_reconnect": true,
    "topic": "",
    "qos": 0
  },
  "publication": {
    "url": "",
    "username": "",
    "password": "",
    "cert_path": "",
    "keep_alive": 5,
    "auto_reconnect": true,
    "topic": "",
    "qos": 0,
    "retain": false
  }
}
```

1. MQTT URI is like `mqtt[s]://host:port`

## Sponsors

<a href="https://www.jetbrains.com/" target="_blank"><img src="https://seppiko.org/images/jetbrains.png" alt="JetBrians" width="100px"></a>
