/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lib

import (
  "crypto/tls"
  "crypto/x509"
  "os"
)

func NewTlsConfig(caCertPath string) (*tls.Config, error) {
  certPool := x509.NewCertPool()

  ca, err := os.ReadFile(caCertPath)
  if err != nil {
    return nil, err
  }

  certPool.AppendCertsFromPEM(ca)

  tlsConf := &tls.Config{
    RootCAs:            certPool,
    InsecureSkipVerify: true,
  }

  return tlsConf, nil
}

func NewTlsConfigWithClient(caPath string, clientCertPath string, clientKeyPath string) (*tls.Config, error) {
  certPool := x509.NewCertPool()

  ca, err := os.ReadFile(caPath)
  if err != nil {
    return nil, err
  }
  certPool.AppendCertsFromPEM(ca)

  clientKeyPair, err := tls.LoadX509KeyPair(clientCertPath, clientKeyPath)
  if err != nil {
    return nil, err
  }

  tlsConf := &tls.Config{
    RootCAs:            certPool,
    ClientAuth:         tls.NoClientCert,
    ClientCAs:          nil,
    InsecureSkipVerify: true,
    Certificates:       []tls.Certificate{clientKeyPair},
  }

  return tlsConf, nil
}
