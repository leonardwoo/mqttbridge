/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lib

import (
  "encoding/json"
  "fmt"
  "time"
)

func Uint2Duration(duration uint16) time.Duration {
  return time.Duration(duration)
}

func ToJson(v interface{}) string {
  b, err := json.Marshal(v)
  if err != nil {
    return ""
  }
  return string(b)
}

func MqttDebug(msg *MqttMsg) {
  jbs, err := json.Marshal(&msg)
  if err != nil {
    panic(err.Error())
  }
  fmt.Println("MQTT: " + string(jbs))
}
