/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lib

import (
  "encoding/json"
  "os"
)

type Config struct {
  ClientId     string   `json:"client_id" default:""`
  Subscription MqttConf `json:"subscription"`
  Publication  MqttConf `json:"publication"`
}

type MqttConf struct {
  URL           string `json:"url" default:""`
  Username      string `json:"username" default:""`
  Password      string `json:"password" default:""`
  CACertPath    string `json:"cert_path" default:""`
  KeepAlive     uint16 `json:"keep_alive" default:"5"`
  AutoReconnect bool   `json:"auto_reconnect" default:"true"`
  Topic         string `json:"topic" default:""`
  Qos           uint8  `json:"qos" default:"0"`
  Retain        bool   `json:"retain" default:"false"`
}

func LoadConfig(pathname string) (Config, error) {
  var config Config
  buf, err := os.ReadFile(pathname)
  if err != nil {
    return config, err
  }
  err = json.Unmarshal(buf, &config)
  return config, err
}
