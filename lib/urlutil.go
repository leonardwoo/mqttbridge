/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lib

import (
  "fmt"
  "net/url"
  "regexp"
  "strconv"
  "strings"
)

const (
  IpRegex         = "(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])"
  DomainRegex     = "(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)+([A-Za-z]|[A-Za-z][A-Za-z0-9\\-]*[A-Za-z0-9])"
  IpOrDomainRegex = "(" + IpRegex + "|" + DomainRegex + ")"
  PortRegex       = "([0-9]|[1-9]\\d{1,3}|[1-5]\\d{4}|6[0-4]\\d{3}|65[0-4]\\d{2}|655[0-2]\\d|6553[0-5])"
)

func MqttUrlParser(mqttUrl string) string {
  u, err := url.Parse(mqttUrl)
  if err != nil {
    return ""
  }

  h := strings.Split(u.Host, ":")
  hostname := h[0]
  port, _ := strconv.ParseInt(h[1], 0, 64)

  if u.Scheme == "mqtt" {
    return fmt.Sprintf("tcp://%s:%d", hostname, port)
  }

  if u.Scheme == "mqtts" {
    return fmt.Sprintf("ssl://%s:%d", hostname, port)
  }

  return ""
}

func VerifyMqttUrl(mqttUrl string) bool {
  if mqttUrl == "" {
    return false
  }

  m, err := regexp.MatchString("mqtts?://"+IpOrDomainRegex+":"+PortRegex, mqttUrl)
  if err != nil {
    return false
  }
  return m
}
