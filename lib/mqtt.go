/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lib

import (
  "fmt"
  "strings"
  "sync"
  "time"

  paho "github.com/eclipse/paho.mqtt.golang"
)

type MqttMsg struct {
  MessageId uint16
  Topic     string
  Qos       uint8
  Duplicate bool
  Retain    bool
  Payload   []uint8
}

var messageSubHandler paho.MessageHandler = func(client paho.Client, msg paho.Message) {
  MsgHandler(msg.MessageID(), msg.Topic(), msg.Qos(), msg.Retained(), msg.Duplicate(), msg.Payload())
}

//var messagePubHandler paho.MessageHandler = func(client paho.Client, msg paho.Message) {
//}

var connectHandler paho.OnConnectHandler = func(client paho.Client) {
  reader := client.OptionsReader()
  urls := reader.Servers()
  fmt.Printf("Server %s Connected/n", urls[0].Host)
}

var connectLostHandler paho.ConnectionLostHandler = func(client paho.Client, err error) {
  reader := client.OptionsReader()
  urls := reader.Servers()
  fmt.Printf("Server %s connect lost: %v/n", urls[0].Host, err)
}

func mqttOpts(mqtt *MqttConf, clientId string) *paho.ClientOptions {
  uri := MqttUrlParser(mqtt.URL)
  if uri == "" {
    panic("MQTT URI (" + mqtt.URL + ") error")
  }

  opts := paho.NewClientOptions()
  opts.AddBroker(uri)
  opts.SetClientID(clientId)
  opts.SetUsername(mqtt.Username)
  opts.SetPassword(mqtt.Password)

  if strings.HasPrefix(mqtt.URL, "mqtts") {
    if mqtt.CACertPath == "" {
      panic("Not found CA certificate file")
    }
    tls, err := NewTlsConfig(mqtt.CACertPath)
    if err != nil {
      panic(err)
    }
    opts.SetTLSConfig(tls)
  }

  opts.SetKeepAlive(Uint2Duration(mqtt.KeepAlive) * time.Second)
  opts.SetCleanSession(true)
  opts.SetAutoReconnect(mqtt.AutoReconnect)
  opts.SetOnConnectHandler(connectHandler)
  opts.SetConnectionLostHandler(connectLostHandler)
  return opts
}

var (
  pubClient  paho.Client
  mqttSubOpt MqttMsg
  mqttPubOpt MqttMsg
)

func sub(client paho.Client) {
  if client.IsConnectionOpen() {
    token := client.Subscribe(mqttSubOpt.Topic, mqttSubOpt.Qos, messageSubHandler)
    //token := client.SubscribeMultiple(filter, messageSubHandler)
    token.Wait()
  }
}

// filter[topic]=qos
func multiSub(client paho.Client, filter map[string]uint8) {
  if client.IsConnectionOpen() {
    token := client.SubscribeMultiple(filter, messageSubHandler)
    token.Wait()
  }
}

func pub(msg []uint8) {
  if pubClient.IsConnectionOpen() {
    token := pubClient.Publish(mqttPubOpt.Topic, mqttPubOpt.Qos, mqttPubOpt.Retain, msg)
    token.Wait()
  }
}

func MsgHandler(messageId uint16, topic string, qos uint8, retain bool, duplicate bool, payload []uint8) {
  //msg := MqttMsg{MessageId: messageId, Topic: topic, Qos: qos, Duplicate: duplicate, Retain: retain, Payload: payload}
  //MqttDebug(&msg)
  pub(payload)
}

func BridgeMessage(mqttSub *MqttConf, mqttPub *MqttConf, clientId string) {

  mqttSubOpt = MqttMsg{Topic: mqttSub.Topic, Qos: mqttSub.Qos}
  subOpts := mqttOpts(mqttSub, clientId)
  subClient := paho.NewClient(subOpts)
  if subToken := subClient.Connect(); subToken.Wait() && subToken.Error() != nil {
    panic(subToken.Error())
  }

  mqttPubOpt = MqttMsg{Topic: mqttPub.Topic, Qos: mqttPub.Qos, Retain: mqttPub.Retain}
  pubOpts := mqttOpts(mqttPub, clientId)
  //pubOpts.SetDefaultPublishHandler(messagePubHandler)
  pubClient = paho.NewClient(pubOpts)
  if pubToken := pubClient.Connect(); pubToken.Wait() && pubToken.Error() != nil {
    panic(pubToken.Error())
  }

  defer subClient.Disconnect(250)
  defer pubClient.Disconnect(300)

  var wg sync.WaitGroup
  wg.Add(1)

  go func() {
    sub(subClient)
  }()

  wg.Wait()
}
