/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
  "flag"
  "fmt"
  "mqttbridge/lib"
  "os"
  "strings"
)

// Author: Leonard Woo
var (
  version      = lib.GetVersion()
  conf         = "config.json"
  printVersion = false
  printHelp    = false
)

func help() {
  fmt.Print(`Usage: mqttbridge [OPTIONS]

OPTIONS:
  -c <config.json>    config file
  -v                  output the version information
  -h                  display help for command
`)
}

func init() {
  flag.StringVar(&conf, "c", conf, "config file")
  flag.BoolVar(&printVersion, "v", false, "Print Version")
  flag.BoolVar(&printHelp, "h", false, "Print Help")
}

func main() {
  flag.Parse()

  if printVersion {
    fmt.Printf("v%s", version)
    os.Exit(0)
  }

  if printHelp {
    help()
    os.Exit(0)
  }

  if len(conf) < 1 || !strings.HasSuffix(conf, ".json") {
    fmt.Println("Config file is not JSON file")
    flag.Usage()
    os.Exit(1)
  }

  config, err := lib.LoadConfig(conf)
  if err != nil {
    panic("Config file load failed")
  }

  if config.Subscription.URL == "" || config.Publication.URL == "" {
    fmt.Println("MQTT url not found")
    os.Exit(0)
  }
  if !lib.VerifyMqttUrl(config.Subscription.URL) || !lib.VerifyMqttUrl(config.Publication.URL) {
    fmt.Println("MQTT uri must be like mqtt://hostname:port or mqtts://hostname:port ")
    os.Exit(0)
  }

  lib.BridgeMessage(&config.Subscription, &config.Publication, config.ClientId)
}
