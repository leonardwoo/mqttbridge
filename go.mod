module mqttbridge

go 1.22

require (
	github.com/eclipse/paho.mqtt.golang v1.4.3
)

require (
	github.com/gorilla/websocket v1.5.0 // indirect
	golang.org/x/net v0.20.0 // indirect
	golang.org/x/sync v0.6.0 // indirect
)
